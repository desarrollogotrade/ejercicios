/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.educacion.ejercicios;

/**
 *
 * @author Developer
 */
public class Iteracion {
    
    public static void main(String[] args) {
         for (int contador = 0; contador <= 100000; contador++) {
            System.out.printf("%d! = %d\n", contador, factorialIteractivo(contador));
        }
    }
     
    public static long factorialIteractivo( long numero )
    {
        long resultado = 1;
        // declaración iterativa del método factorial
        for ( long i = numero; i >= 1; i-- )
            resultado *= i;
        return resultado;
    } // fin del m
}
